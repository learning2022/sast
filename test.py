#!/usr/bin/env python3

import json 
# import json2html
from json2html import *
import webbrowser
import os

with open('./test_report.json') as f:
    d = json.load(f)
    scanOutput = json2html.convert(json=d)
    htmlReportFile = "./sast_report.html" 
    with open(htmlReportFile, 'w') as htmlfile:
        htmlfile.write(str(scanOutput))
        print("Json file is converted into html successfully...")
        webbrowser.open(htmlReportFile)
        absolute_path = os.path.abspath(__file__)
        print("Report Path: " + 'file:///' + os.path.dirname(absolute_path) + "/sast_report.html") 